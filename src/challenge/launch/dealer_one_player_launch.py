from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='challenge',
            executable='player',
        ),
        Node(
            package='challenge',
            executable='dealer',
        )
    ])
