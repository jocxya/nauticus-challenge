#include <gtest/gtest.h>
#include <iostream>
#include <vector>
#include "challenge/Deck.Deck.hpp"

TEST(DeckOfCardsTesting, AddOneCardToDeck)
{
  Deck::CDeck deck;
  EXPECT_TRUE(deck.Size()==0);
  Deck::CCard card(Deck::CCard::Hearts,Deck::CCard::A);
  deck.Add(card);
  EXPECT_TRUE(deck.Size()==1);
}

TEST(DeckOfCardsTesting, DrawCardFromDeck)
{
  Deck::CDeck deck;
  Deck::CCard card1(Deck::CCard::Hearts,Deck::CCard::A);
  Deck::CCard card2(Deck::CCard::Clubs,Deck::CCard::_5);
  Deck::CCard card3(Deck::CCard::Spades,Deck::CCard::K);
  deck.Add(card1);
  deck.Add(card2);
  deck.Add(card3);
  EXPECT_TRUE(deck.Size()==3);
  Deck::CCard oDrawnCard = deck.DrawFromTop();
  EXPECT_TRUE(deck.Size()==2);
  EXPECT_TRUE(oDrawnCard.GetSuit()==Deck::CCard::Spades);
  EXPECT_TRUE(oDrawnCard.GetRank()==Deck::CCard::K);
}

TEST(DeckOfCardsTesting,CuttingDeck)
{
  using namespace Deck;
  std::vector<CCard> vCards{
			    CCard(CCard::Spades,CCard::_8),
			    CCard(CCard::Spades,CCard::J),
			    CCard(CCard::Hearts,CCard::_3),
			    CCard(CCard::Hearts,CCard::Q),
			    CCard(CCard::Clubs,CCard::A),
                            CCard(CCard::Clubs,CCard::_2),
                            CCard(CCard::Diamonds,CCard::_4),
                            CCard(CCard::Diamonds,CCard::_9),
			    CCard(CCard::Diamonds,CCard::_5)
  };

  CDeck deck;
  for(CCard& card : vCards)
    {
      deck.Add(card);
    }
  deck.Cut();
  unsigned iHalfSize = deck.Size()/2;
 
  unsigned idx = iHalfSize-1;  
  while(deck.Size()!=0)
    {
      CCard card = deck.DrawFromTop();
      //std::cout<<"Deck: "<<card<<" - Vector: "<<vCards[idx]<<std::endl;
      EXPECT_TRUE(card == vCards[idx]);
      if(idx==0)
	{
	  idx=vCards.size();
	}
      idx--;
    }
}

TEST(DeckOfCardsTesting,ShuffleVerifyTopAndBottomCards)
{
  using namespace Deck;
  std::vector<CCard> vCards{
                            CCard(CCard::Spades,CCard::_8),
                            CCard(CCard::Spades,CCard::J),
                            CCard(CCard::Hearts,CCard::_3),
                            CCard(CCard::Hearts,CCard::Q),
                            CCard(CCard::Clubs,CCard::A),
                            CCard(CCard::Clubs,CCard::_2),
	                    CCard(CCard::Diamonds,CCard::_4),
                            CCard(CCard::Diamonds,CCard::_9),
	                    CCard(CCard::Diamonds,CCard::_5)
  };

  CDeck deck;
  for(CCard& card : vCards)
    {
      deck.Add(card);
    }
  deck.RiffleShuffle();

  // Check that the top card is in the one of the two top spots
  CCard cTopCard = vCards.back();
  bool bTopCardFound = false;
  CCard cBottomCard = vCards.front();
  bool bBottomCardFound	= false;
  //std::cout<<"Top Card: "<<cTopCard<<std::endl;
  //std::cout<<"Bottom Card: "<<cBottomCard<<std::endl;
  for(unsigned i=0;i<vCards.size();i++)
    {
      if(i<2)
	{
	  if(deck.DrawFromTop()==cTopCard)
	    {
	      bTopCardFound = true;
	    }
	}
      else if(i>=vCards.size()-2 && i<vCards.size())
	{
	  if(deck.DrawFromTop()==cBottomCard)
	    {
	      bBottomCardFound = true;
	    }
	}
      else
	{
	  deck.DrawFromTop();
	}
    }

  EXPECT_TRUE(bTopCardFound && bBottomCardFound);
}

TEST(DeckOfCardsTesting,GenerateStdDeck)
{
  Deck::CDeck deck;
  deck.InitializeStandardDeck();
  EXPECT_TRUE(deck.Size()==52);
}
