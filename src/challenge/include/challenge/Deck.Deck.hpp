#pragma once
#include <list>
#include <iostream>
#include <string>

namespace Deck{
  class CCard
  {
  public:
    enum Suit {Hearts, Clubs, Diamonds, Spades};
    enum Rank {_2,_3,_4,_5,_6,_7,_8,_9,_10,J,Q,K,A};

    CCard(Suit sSuit, Rank rRank);
    Suit GetSuit() const;
    Rank GetRank() const;
    bool operator==(const CCard& rhs);
    friend std::ostream& operator<<(std::ostream& cin, const CCard& card);
    static std::string Card2Str(const CCard& cCard);
    static std::string Suit2Str(const Suit& sSuit);
    static std::string Rank2Str(const Rank& rRank);
    static CCard GenRandCard();
    
  private:
    Suit suit;
    Rank rank;
  };

  class CDeck
  {
  public:
    void Add(CCard& oCard);
    CCard DrawFromTop();
    void Cut();
    void RandomShuffle();
    void RiffleShuffle();
    unsigned Size();
    void InitializeStandardDeck();
    void ShowHand();
  private:
    std::list<CCard> m_lDeck;
  };
}//namespace Deck
