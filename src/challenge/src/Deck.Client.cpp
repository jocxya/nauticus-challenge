#include <memory>
#include <chrono>
#include "rclcpp/rclcpp.hpp"
#include "custom_interfaces/srv/deal_card.hpp"
#include "challenge/Deck.Deck.hpp"

using namespace std::chrono_literals;

Deck::CDeck deck;

int main(int argc, char **argv)
{
  rclcpp::init(argc, argv);

  //Creating client node
  std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("player_client");
  rclcpp::Client<custom_interfaces::srv::DealCard>::SharedPtr client =
    node->create_client<custom_interfaces::srv::DealCard>("deal_card");

  // Request dealer to serve five cards
  for(unsigned i=0;i<5;i++)
    {
      auto request = std::make_shared<custom_interfaces::srv::DealCard::Request>();
  
      while (!client->wait_for_service(1s)) {
	if (!rclcpp::ok()) {
	  RCLCPP_ERROR(rclcpp::get_logger("rclcpp"),
		       "Interrupted while waiting for the DealCard service. Exiting.");
	  return 0;
	}
	RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "DealCard service not available, waiting again...");
      }

      auto result = client->async_send_request(request);
      // Wait for the result.
      if (rclcpp::spin_until_future_complete(node, result) ==
	  rclcpp::FutureReturnCode::SUCCESS)
	{
	  if(result.get()->success)
	    {
	      Deck::CCard card(static_cast<Deck::CCard::Suit>(result.get()->suit),
			       static_cast<Deck::CCard::Rank>(result.get()->rank));
	      deck.Add(card);
	
	      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Received Card: %s", Deck::CCard::Card2Str(card).c_str());
	      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Deck Size: %d", deck.Size());
	    }
	  else
	    {
	      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Dealer failed to serve card: %s",
			  result.get()->errormsg.c_str());
	    }
	}
      else
	{
	  RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Failed to call service deal_card");
	}
    }
  deck.ShowHand();
  rclcpp::shutdown();
}
