#include <memory>
#include "rclcpp/rclcpp.hpp"
#include "custom_interfaces/srv/deal_card.hpp"
#include "challenge/Deck.Deck.hpp"

Deck::CDeck deck;

void DealCard(const std::shared_ptr<custom_interfaces::srv::DealCard::Request> request,
	      std::shared_ptr<custom_interfaces::srv::DealCard::Response>      response)
{
  try{
    Deck::CCard card = deck.DrawFromTop();

    response->success = true;
    response->errormsg = "No Error";
    response->suit = static_cast<int>(card.GetSuit());
    response->rank = static_cast<int>(card.GetRank());
    
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Dealing card: %s",Deck::CCard::Card2Str(card).c_str());
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Deck Size: %d",deck.Size());
  }
  catch(const std::runtime_error& e)
    {
      response->success = false;
      response->errormsg = e.what();
      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Error Dealing Card: %s",response->errormsg.c_str());
    }
}

int main(int argc, char **argv)
{
  // Generate the 52-card standard deck
  deck.InitializeStandardDeck();
  std::cout<<"Deck Size: "<<deck.Size()<<std::endl;

  // Shuffle and Cut a 10 times
  unsigned i;
  for(i=0;i<10;i++)
    {
      deck.RiffleShuffle();
      deck.Cut();
    }

  // Draw five cards
  //for(i=0;i<5;i++)
  //  {
  //    std::cout<<deck.DrawFromTop()<<std::endl;
  //  }

  rclcpp::init(argc, argv);
  std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("dealer_server");

  rclcpp::Service<custom_interfaces::srv::DealCard>::SharedPtr service =
    node->create_service<custom_interfaces::srv::DealCard>("deal_card",&DealCard);

  RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"Ready to deal cards");

  rclcpp::spin(node);
  rclcpp::shutdown();
}
