#include <stdexcept>
#include <algorithm>
#include <random>
#include <vector>
#include <functional>
#include "challenge/Deck.Deck.hpp"

namespace Deck
{
  CCard::CCard(Suit sSuit, Rank rRank):
    suit(sSuit), rank(rRank)
  {}

  CCard::Suit CCard::GetSuit() const
  {
    return suit;
  }

  CCard::Rank CCard::GetRank() const
  {
    return rank;
  }
  
  bool CCard::operator==(const CCard& rhs)
  {
    return suit==rhs.suit && rank==rhs.rank;
  }
  

  std::ostream& operator<<(std::ostream& cin, const CCard& card)
  {
    cin<<CCard::Card2Str(card);
    return cin;
  }

  std::string CCard::Card2Str(const CCard& cCard)
  {
    return CCard::Rank2Str(cCard.GetRank())+" of "+CCard::Suit2Str(cCard.GetSuit());
  }
  
  std::string CCard::Suit2Str(const Suit& sSuit)
  {
    switch(sSuit)
      {
      case Hearts:
	return "Hearts";
      case Clubs:
	return "Clubs";
      case Diamonds:
	return "Diamonds";
      case Spades:
	return "Spades";
      default:
	return "Unknown Suit";
      }
  }

  std::string CCard::Rank2Str(const Rank& rRank)
  {
    switch(rRank)
      {
      case _2:
	return "Two";
      case _3:
	return "Three";
      case _4:
	return "Four";
      case _5:
	return "Five";
      case _6:
	return "Six";
      case _7:
	return "Seven";
      case _8:
	return "Eight";
      case _9:
	return "Nine";
      case _10:
	return "Ten";
      case J:
	return "Jack";
      case Q:
	return "Queen";
      case K:
	return "King";
      case A:
	return "Ace";
      default:
	return "Unknown Rank";
      }
  }

  CCard CCard::GenRandCard()
  {
    unsigned uSuit = rand() % 4;
    unsigned uRank = rand() % 13;
    return CCard(static_cast<Suit>(uSuit),static_cast<Rank>(uRank));
  }

  void CDeck::Add(CCard& oCard)
  {
    m_lDeck.push_back(oCard);
  }

  CCard CDeck::DrawFromTop()
  {
    if(!m_lDeck.empty())
      {
	// Copy the card at the top
	CCard cCardAtTop = m_lDeck.back();
	//std::cout<<cCardAtTop<<std::endl;
	
	// Remove it from deck
	m_lDeck.pop_back();
	// Return the copy
	return cCardAtTop;
      }
    else
      {
	throw std::runtime_error("The deck is empty. Cannot draw a card.");
      }
  }

  void CDeck::Cut()
  {
    if(!m_lDeck.empty())
      {
	// Calculate the position at middle of the deck
	unsigned int iHalfSize = m_lDeck.size()/2;
	// Move to the middle of the deck
	std::list<CCard>::iterator it = m_lDeck.begin();
	std::advance(it,iHalfSize);
	// Re-point the internal pointers of the list nodes so that the
	// cards in last part of the list are moved to the beginning of the list.
	// No elements are copied.
	m_lDeck.splice(m_lDeck.begin(),m_lDeck,it,m_lDeck.end());
      }
    else
      {
	throw std::runtime_error("The deck is empty. Cannot cut.");
      }
  }

  void CDeck::RandomShuffle()
  {
    if(!m_lDeck.empty())
      {
	// Create a vector with references to the objects in the deck list
	std::vector<std::reference_wrapper<CCard>> vDeck(m_lDeck.begin(),m_lDeck.end());
	// Create random number generator engine
	std::random_device oRandDevice;
	std::default_random_engine oRandGenEngine(oRandDevice());
	// Shuffle the vector of references
	std::shuffle(vDeck.begin(),vDeck.end(),oRandGenEngine);
	// Create a new list from the shuffled vector
	std::list<CCard> lShuffledList;
	for(std::reference_wrapper<CCard>& rwCard : vDeck)
	  {
	    // Transfer card objects by moving them to the new shuffled list
	    lShuffledList.push_back(std::move(rwCard.get()));
	  }
	// Update the deck list by swaping with the shuffled list
	m_lDeck.swap(lShuffledList);
      }
    else
      {
	throw std::runtime_error("The deck is empty. Cannot shuffle.");
      }
  }

  void CDeck::RiffleShuffle()
  {
    if(!m_lDeck.empty())
      {
	// Make a copy of the deck
	std::list<CCard> lLeftDeck(m_lDeck);
	// Calculate the position at middle of the deck                                           
        unsigned int iHalfSize = m_lDeck.size()/2;
        // Move to the middle of the deck                                                         
        std::list<CCard>::iterator it = lLeftDeck.begin();
        std::advance(it,iHalfSize);
        // Split left deck and create the right deck
	std::list<CCard> lRightDeck;
        lRightDeck.splice(lRightDeck.begin(),lLeftDeck,it,lLeftDeck.end());
	// Create shuffled deck
	std::list<CCard> lShuffledList;
	bool bDrawFromRight = true;
	while(!lRightDeck.empty() || !lLeftDeck.empty())
	  {
	    if(bDrawFromRight)
	      {
	        lShuffledList.splice(lShuffledList.end(),
				     lRightDeck,lRightDeck.begin(),std::next(lRightDeck.begin()));
	      }
	    else
	      {
		lShuffledList.splice(lShuffledList.end(),
				     lLeftDeck,lLeftDeck.begin(),std::next(lLeftDeck.begin()));
	      }
	    
	    //Update side from which to draw a card next
	    bDrawFromRight=!bDrawFromRight;
	  }

	//Swap the shuffled list with the original member object list deck
	m_lDeck.swap(lShuffledList);
      }
    else
      {
	throw std::runtime_error("The deck is empty. Cannot shuffle.");
      }
  }

  unsigned CDeck::Size()
  {
    return m_lDeck.size();
  }

  void CDeck::InitializeStandardDeck()
  {
    m_lDeck.clear();
    const unsigned uTotalSuit = 4;
    const unsigned uTotalRank = 13;

    for(unsigned i=0;i<uTotalSuit;i++)
      {
	for(unsigned j=0;j<uTotalRank;j++)
	  {
	    CCard card = CCard(static_cast<CCard::Suit>(i),static_cast<CCard::Rank>(j));
	    Add(card);
	  }
      }
  }

  void CDeck::ShowHand()
  {
    std::cout<<"Total Deck Size: "<<Size()<<std::endl;
    for(auto const &cCard : m_lDeck)
      {
	std::cout<<cCard<<std::endl;
      }
  }

}//namespace Deck
